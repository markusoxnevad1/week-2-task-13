﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_13
{
    //  Program that generates an array with random ints
    //  You can also add/retrieve random elements within the list.
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<int> intList = new ChaosArray<int>();
            Console.WriteLine("This is a chaotic array, where numbers have been placed in random order");
            Console.WriteLine();
            for (int i = 0; i <10; i++)
            {
                intList.chaosarrayAdd(i);
            }
            intList.chaosarrayPrint();
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("Now adds a new Element: 2 in a random spot in the ChaosArray");
            intList.chaosarrayAdd(2);
            intList.chaosarrayPrint();
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("Now we will retrieve an random item from the list");
            Console.WriteLine(intList.chaosRetrieve());


    
        }
    }
}
