﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_13
{
    //  Class of ChaosArray
    public class ChaosArray<T>
    {
        private T[] chaosArray;
        private Random randomIndex = new Random();
        //  Sets default size to 10
        public ChaosArray()
        {
            chaosArray = new T[10];
        }
        //  Method that adds an element randomly in the array
        public void chaosarrayAdd(T item)
        {
            bool retry = false;
            do
            {
                int addrandom;
                T randomizer;
                addrandom = randomIndex.Next(chaosArray.Length);
                try
                {
                    randomizer = chaosArray[addrandom];
                    if (randomizer.Equals(default(T)))
                    {
                        chaosArray[addrandom] = item;
                    }
                    //  If the index you are trying to add an item too is occupied in the array
                    //  It throws the exception away
                    else
                    {
                        throw new Exception($"Element {item.ToString()} was not added, as spot {addrandom+1} already is taken");
                    }

                }
                //  The exception that was thrown away, is caught and printed out
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            } while (retry);
        }
        //  Prints out the array, spots in the array that are not assigned an element are marked as VACANT
        public void chaosarrayPrint()
        {
            foreach (T item in chaosArray)
            {
                if (item.Equals(default(T)))
                {
                    Console.Write("VACANT");
                }
                Console.WriteLine(item.ToString());
            }
        }
        //  Retrieves a random element in the array
        public T chaosRetrieve()
        {
            int getrandom;
            getrandom = randomIndex.Next(chaosArray.Length);
            T item = chaosArray[getrandom];
            return item;
        }
    }
}
